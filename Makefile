all: LD_PRELOAD_xpath.so

# CFLAGS += -g
CFLAGS += -O2
CFLAGS += -Wall

LD_PRELOAD_%.so: LD_PRELOAD_%.c
	$(CC) $(CFLAGS) -fPIC -shared -o $@ $< -ldl
