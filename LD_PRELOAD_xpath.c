// SPDX-FileCopyrightText: 2021 Emulation-as-a-Service contributors
// SPDX-License-Identifier: CC0-1.0

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

int starts_with(const char *string, const char *prefix) {
  return !strncmp(string, prefix, strlen(prefix));
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
  static int (*connect)();
  if (!connect) connect = dlsym(RTLD_NEXT, "connect");
  if (addr->sa_family == AF_UNIX) {
    char *path = ((struct sockaddr_un *)addr)->sun_path;
    if (path[0] == 0) path += 1;
    if (starts_with(path, "/tmp/.X11-unix/X")) {
      const char *xpath = getenv("XPATH");
      if (xpath && xpath[0]) strcpy(path, xpath);
    }
  }
  return connect(sockfd, addr, addrlen);
}
